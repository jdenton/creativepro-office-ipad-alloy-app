function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.subPanel = Ti.UI.createView({
        borderRadius: 10,
        backgroundImage: "images/bgPanel.png",
        backgroundRepeat: true,
        zIndex: 3,
        id: "subPanel"
    });
    $.__views.subPanel && $.addTopLevelView($.__views.subPanel);
    $.__views.subPanelContent = Ti.UI.createView({
        layout: "vertical",
        top: "0dp",
        left: "0dp",
        width: "482dp",
        id: "subPanelContent"
    });
    $.__views.subPanel.add($.__views.subPanelContent);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;