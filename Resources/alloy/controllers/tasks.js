function Controller() {
    function test(e) {
        0 == e.index ? Ti.App.fireEvent("openSubPanel") : Ti.App.fireEvent("openSubPanel");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.tasks = Ti.UI.createView({
        width: "100%",
        height: "70dp",
        left: "4dp",
        backgroundColor: "#e1e8ef",
        id: "tasks"
    });
    $.__views.tasks && $.addTopLevelView($.__views.tasks);
    $.__views.__alloyId10 = Ti.UI.createLabel({
        left: "24dp",
        font: {
            fontFamily: "Helvetica-Light",
            fontSize: 32
        },
        shadowColor: "#ffffff",
        shadowOffset: {
            x: 0,
            y: 1
        },
        text: "Tasks",
        id: "__alloyId10"
    });
    $.__views.tasks.add($.__views.__alloyId10);
    $.__views.__alloyId11 = Ti.UI.createView({
        left: "300dp",
        width: "178dp",
        id: "__alloyId11"
    });
    $.__views.tasks.add($.__views.__alloyId11);
    var __alloyId13 = [];
    var __alloyId14 = {
        title: "Open",
        width: "30",
        ns: "Alloy.Abstract"
    };
    __alloyId13.push(__alloyId14);
    var __alloyId15 = {
        title: "Close",
        width: "30",
        ns: "Alloy.Abstract"
    };
    __alloyId13.push(__alloyId15);
    $.__views.btnBarHeader = Ti.UI.createButtonBar({
        height: "10dp",
        labels: __alloyId13,
        id: "btnBarHeader"
    });
    $.__views.__alloyId11.add($.__views.btnBarHeader);
    test ? $.__views.btnBarHeader.addEventListener("click", test) : __defers["$.__views.btnBarHeader!click!test"] = true;
    $.__views.np = Ti.UI.createButton({
        title: "Open new panel",
        id: "np"
    });
    $.__views.np && $.addTopLevelView($.__views.np);
    $.__views.getStuff = Ti.UI.createButton({
        title: "Get new view",
        id: "getStuff"
    });
    $.__views.getStuff && $.addTopLevelView($.__views.getStuff);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.btnBarHeader!click!test"] && $.__views.btnBarHeader.addEventListener("click", test);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;