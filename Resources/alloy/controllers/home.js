function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundImage: "images/denim.png",
        backgroundRepeat: true,
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.menu = Alloy.createWidget("com.madrocket.ti.slidemenu", "widget", {
        id: "menu",
        __parentSymbol: $.__views.index
    });
    $.__views.menu.setParent($.__views.index);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var mainPanel = Alloy.createController("mainPanel");
    var subPanel = Alloy.createController("subPanel");
    var currentView;
    $.menu.init({
        leftDrawer: Alloy.createController("leftMenu").getView(),
        content: mainPanel.getView(),
        subContent: subPanel.getView()
    });
    Ti.App.addEventListener("menuChange", function(data) {
        var view = Alloy.createController(data.panel).getView();
        mainPanel.mainContent.add(view);
        currentView && mainPanel.mainContent.remove(currentView);
        currentView = view;
    });
    Ti.App.addEventListener("openSubPanel", function() {
        $.menu.openSubPanel();
    });
    Ti.App.addEventListener("closeSubPanel", function() {
        $.menu.closeSubPanel();
    });
    Ti.App.fireEvent("menuChange", {
        panel: "projects"
    });
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;