function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.mainPanel = Ti.UI.createView({
        borderRadius: 10,
        backgroundImage: "images/bgPanel.png",
        backgroundRepeat: true,
        layout: "vertical",
        zIndex: 2,
        id: "mainPanel"
    });
    $.__views.mainPanel && $.addTopLevelView($.__views.mainPanel);
    $.__views.mainContent = Ti.UI.createView({
        layout: "vertical",
        top: "0dp",
        left: "0dp",
        width: "482dp",
        id: "mainContent"
    });
    $.__views.mainPanel.add($.__views.mainContent);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;