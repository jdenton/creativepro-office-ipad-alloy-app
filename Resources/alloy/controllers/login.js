function Controller() {
    function focusPassword() {
        $.password.focus();
    }
    function login(e) {
        e && e.source && _.isFunction(e.source.blur) && e.source.blur();
        if (login.login($.username.value, $.password.value)) {
            Alloy.createController("home").getView().open();
            $.login.close();
        } else alert("login failed");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.login = Ti.UI.createWindow({
        fullscreen: false,
        exitOnClose: true,
        id: "login"
    });
    $.__views.login && $.addTopLevelView($.__views.login);
    $.__views.__alloyId2 = Ti.UI.createView({
        height: "301dp",
        width: "200dp",
        id: "__alloyId2"
    });
    $.__views.login.add($.__views.__alloyId2);
    $.__views.username = Ti.UI.createTextField({
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        width: "200dp",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "130dp",
        hintText: "username",
        returnKeyType: Ti.UI.RETURNKEY_NEXT,
        id: "username"
    });
    $.__views.__alloyId2.add($.__views.username);
    focusPassword ? $.__views.username.addEventListener("return", focusPassword) : __defers["$.__views.username!return!focusPassword"] = true;
    $.__views.password = Ti.UI.createTextField({
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        width: "200dp",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "170dp",
        hintText: "password",
        passwordMask: true,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "password"
    });
    $.__views.__alloyId2.add($.__views.password);
    login ? $.__views.password.addEventListener("return", login) : __defers["$.__views.password!return!login"] = true;
    $.__views.loginButton = Ti.UI.createButton({
        top: "210dp",
        height: "40dp",
        width: "100dp",
        color: "#fff",
        backgroundImage: "/button_normal.png",
        backgroundSelectedImage: "/button_selected.png",
        title: "Login",
        id: "loginButton"
    });
    $.__views.__alloyId2.add($.__views.loginButton);
    login ? $.__views.loginButton.addEventListener("click", login) : __defers["$.__views.loginButton!click!login"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var login = Alloy.Models.login;
    __defers["$.__views.username!return!focusPassword"] && $.__views.username.addEventListener("return", focusPassword);
    __defers["$.__views.password!return!login"] && $.__views.password.addEventListener("return", login);
    __defers["$.__views.loginButton!click!login"] && $.__views.loginButton.addEventListener("click", login);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;