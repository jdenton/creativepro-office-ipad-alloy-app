function Controller() {
    function test(e) {
        0 == e.index ? Ti.App.fireEvent("openSubPanel") : Ti.App.fireEvent("closeSubPanel");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.projects = Ti.UI.createView({
        width: "100%",
        height: "70dp",
        left: "4dp",
        backgroundColor: "#e1e8ef",
        id: "projects"
    });
    $.__views.projects && $.addTopLevelView($.__views.projects);
    $.__views.__alloyId3 = Ti.UI.createLabel({
        left: "24dp",
        font: {
            fontFamily: "Helvetica-Light",
            fontSize: 32
        },
        shadowColor: "#ffffff",
        shadowOffset: {
            x: 0,
            y: 1
        },
        text: "Projects",
        id: "__alloyId3"
    });
    $.__views.projects.add($.__views.__alloyId3);
    $.__views.__alloyId4 = Ti.UI.createView({
        left: "300dp",
        width: "178dp",
        id: "__alloyId4"
    });
    $.__views.projects.add($.__views.__alloyId4);
    var __alloyId6 = [];
    var __alloyId7 = {
        title: "Open",
        width: "100",
        ns: "Alloy.Abstract"
    };
    __alloyId6.push(__alloyId7);
    var __alloyId8 = {
        title: "Close",
        width: "100",
        ns: "Alloy.Abstract"
    };
    __alloyId6.push(__alloyId8);
    $.__views.btnBarHeader = Ti.UI.createButtonBar({
        height: "10dp",
        labels: __alloyId6,
        id: "btnBarHeader"
    });
    $.__views.__alloyId4.add($.__views.btnBarHeader);
    test ? $.__views.btnBarHeader.addEventListener("click", test) : __defers["$.__views.btnBarHeader!click!test"] = true;
    $.__views.np = Ti.UI.createButton({
        title: "Open new panel",
        id: "np"
    });
    $.__views.np && $.addTopLevelView($.__views.np);
    $.__views.getStuff = Ti.UI.createButton({
        title: "Get new view",
        id: "getStuff"
    });
    $.__views.getStuff && $.addTopLevelView($.__views.getStuff);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.btnBarHeader!click!test"] && $.__views.btnBarHeader.addEventListener("click", test);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;