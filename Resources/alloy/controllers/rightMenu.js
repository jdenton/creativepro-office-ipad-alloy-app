function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.rightMenu = Ti.UI.createView({
        width: "250",
        id: "rightMenu"
    });
    $.__views.rightMenu && $.addTopLevelView($.__views.rightMenu);
    $.__views.__alloyId9 = Ti.UI.createLabel({
        text: "I'm the right menu",
        id: "__alloyId9"
    });
    $.__views.rightMenu.add($.__views.__alloyId9);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;