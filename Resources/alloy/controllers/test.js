function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.test = Ti.UI.createView({
        backgroundColor: "blue",
        layout: "vertical",
        id: "test"
    });
    $.__views.test && $.addTopLevelView($.__views.test);
    $.__views.__alloyId16 = Ti.UI.createLabel({
        text: "I'm the new view",
        id: "__alloyId16"
    });
    $.__views.test.add($.__views.__alloyId16);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;