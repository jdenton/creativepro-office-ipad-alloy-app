function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.leftMenu = Ti.UI.createView({
        width: 295,
        left: 0,
        height: Ti.UI.FILL,
        layout: "vertical",
        backgroundColor: "white",
        backgroundRepeat: true,
        zIndex: 1,
        id: "leftMenu"
    });
    $.__views.leftMenu && $.addTopLevelView($.__views.leftMenu);
    $.__views.searchBar = Ti.UI.createSearchBar({
        height: "70dp",
        top: 0,
        barColor: "#000",
        showCancel: false,
        hintText: "Search your office",
        id: "searchBar"
    });
    $.__views.leftMenu.add($.__views.searchBar);
    $.__views.menu1 = Ti.UI.createLabel({
        width: Ti.UI.FILL,
        height: "50dp",
        backgroundImage: "images/bgMenuItem.png",
        backgroundRepeat: true,
        text: "",
        id: "menu1"
    });
    $.__views.leftMenu.add($.__views.menu1);
    $.__views.menuIconProjects = Ti.UI.createLabel({
        width: "32dp",
        height: "32dp",
        left: "25dp",
        backgroundImage: "images/projects.png",
        id: "menuIconProjects"
    });
    $.__views.menu1.add($.__views.menuIconProjects);
    $.__views.__alloyId0 = Ti.UI.createLabel({
        font: {
            fontFamily: "Helvetica-Light",
            fontSize: 20
        },
        color: "#ffffff",
        left: "80dp",
        text: "Projects",
        id: "__alloyId0"
    });
    $.__views.menu1.add($.__views.__alloyId0);
    $.__views.menu2 = Ti.UI.createLabel({
        width: Ti.UI.FILL,
        height: "50dp",
        backgroundImage: "images/bgMenuItem.png",
        backgroundRepeat: true,
        text: "",
        id: "menu2"
    });
    $.__views.leftMenu.add($.__views.menu2);
    $.__views.menuIconTasks = Ti.UI.createLabel({
        width: "32dp",
        height: "32dp",
        left: "25dp",
        backgroundImage: "images/tasks.png",
        id: "menuIconTasks"
    });
    $.__views.menu2.add($.__views.menuIconTasks);
    $.__views.__alloyId1 = Ti.UI.createLabel({
        font: {
            fontFamily: "Helvetica-Light",
            fontSize: 20
        },
        color: "#ffffff",
        left: "80dp",
        text: "Tasks",
        id: "__alloyId1"
    });
    $.__views.menu2.add($.__views.__alloyId1);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.menu1.addEventListener("click", function() {
        this.backgroundImage = "images/bgMenuItemSelected.png";
        Ti.App.fireEvent("menuChange", {
            panel: "projects"
        });
    });
    $.menu2.addEventListener("click", function() {
        Ti.App.fireEvent("menuChange", {
            panel: "tasks"
        });
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;