exports.definition = {
    config: {
        columns: {
            UserId: "INTEGER PRIMARY KEY",
            Email: "TEXT",
            Password: "TEXT",
            AuthKey: "TEXT",
            Permissions: "TEXT",
            UserType: "INTEGER"
        },
        adapter: {
            type: "sql",
            collection_name: "login",
            idAttribute: "UserId"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {
            login: function(username, password) {
                if (username === USERNAME && password === PASSWORD) {
                    this.set({
                        loggedIn: 1,
                        authKey: AUTHKEY
                    });
                    this.save();
                    return true;
                }
                return false;
            },
            logout: function() {
                this.set({
                    loggedIn: 0,
                    authKey: ""
                });
                this.save();
            },
            validateAuth: function() {
                return 1 === this.get("loggedIn") && this.get("authKey") === AUTHKEY ? true : false;
            }
        });
        return Model;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("login", exports.definition, [ function(migration) {
    migration.name = "login";
    migration.id = "20130713000000";
    migration.up = function(migrator) {
        migrator.createTable({
            columns: {
                UserId: "INTEGER PRIMARY KEY",
                Email: "TEXT",
                Password: "TEXT",
                AuthKey: "TEXT",
                Permissions: "TEXT",
                UserType: "INTEGER"
            },
            adapter: {
                type: "sql",
                db_file: "cpo",
                collection_name: "login"
            }
        });
    };
    migration.down = function(migrator) {
        migrator.dropTable("login");
    };
} ]);

collection = Alloy.C("login", exports.definition, model);

exports.Model = model;

exports.Collection = collection;