exports.definition = {
    config: {
        columns: {
            TimesheetId: "INTEGER PRIMARY KEY",
            ProjectId: "INTEGER",
            ProjectTitle: "TEXT",
            TaskId: "INTEGER",
            TaskTitle: "TEXT",
            Start: "INTEGER",
            Stop: "INTEGER",
            ElapsedTime: "REAL",
            DateClockStart: "TEXT",
            DateClockEnd: "TEXT",
            Billable: "INTEGER",
            Comments: "TEXT"
        },
        adapter: {
            type: "sql",
            collection_name: "timesheet",
            idAttribute: "TimesheetId"
        }
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("timesheet", exports.definition, [ function(migration) {
    migration.name = "timesheet";
    migration.id = "20130713000000";
    migration.up = function(migrator) {
        migrator.createTable({
            columns: {
                TimesheetId: "INTEGER PRIMARY KEY",
                ProjectId: "INTEGER",
                ProjectTitle: "TEXT",
                TaskId: "INTEGER",
                TaskTitle: "TEXT",
                Start: "INTEGER",
                Stop: "INTEGER",
                ElapsedTime: "REAL",
                DateClockStart: "TEXT",
                DateClockEnd: "TEXT",
                Billable: "INTEGER",
                Comments: "TEXT"
            },
            adapter: {
                type: "sql",
                db_file: "cpo",
                collection_name: "timesheet"
            }
        });
    };
    migration.down = function(migrator) {
        migrator.dropTable("timesheet");
    };
} ]);

collection = Alloy.C("timesheet", exports.definition, model);

exports.Model = model;

exports.Collection = collection;