function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.madrocket.ti.slidemenu/" + s : s.substring(0, index) + "/com.madrocket.ti.slidemenu/" + s.substring(index + 1);
    return path;
}

function Controller() {
    function getPanelAnimation(left) {
        return Ti.UI.createAnimation({
            left: left,
            curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
            duration: 150
        });
    }
    new (require("alloy/widget"))("com.madrocket.ti.slidemenu");
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.slideMenu = Ti.UI.createView({
        id: "slideMenu"
    });
    $.__views.slideMenu && $.addTopLevelView($.__views.slideMenu);
    $.__views.leftDrawer = Ti.UI.createView({
        top: 0,
        left: 0,
        zIndex: 1,
        id: "leftDrawer"
    });
    $.__views.slideMenu.add($.__views.leftDrawer);
    $.__views.content = Ti.UI.createView({
        top: 0,
        left: 287,
        zIndex: 10,
        width: 487,
        id: "content"
    });
    $.__views.slideMenu.add($.__views.content);
    $.__views.subContent = Ti.UI.createView({
        top: 0,
        left: 700,
        opacity: 0,
        zIndex: 11,
        width: 487,
        id: "subContent"
    });
    $.__views.slideMenu.add($.__views.subContent);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var sub_panel_is_opened = false;
    var drawer = {
        is_opened: false,
        initialize: function(content) {
            this.setWidth(content.width);
            this.add(content);
        },
        openDrawer: function() {
            this.fireEvent("open");
            $.content.animate(getPanelAnimation(287));
            this.is_opened = true;
        },
        closeDrawer: function() {
            this.fireEvent("close");
            $.content.animate(getPanelAnimation(75));
            this.is_opened = false;
        },
        toggleDrawer: function() {
            this.is_opened ? this.closeDrawer() : this.openDrawer();
        }
    };
    _.extend($.leftDrawer, drawer, {});
    $.content.addEventListener("touchmove", function() {});
    $.leftDrawer.addEventListener("open", function() {
        $.trigger("open:[left]");
    });
    $.leftDrawer.addEventListener("close", function() {
        $.trigger("close:[left]");
    });
    var subPanel = {
        initialize: function(panelContent) {
            this.setWidth(panelContent.width);
            this.add(panelContent);
        },
        openPanel: function() {
            this.fireEvent("openPanel");
            $.subContent.opacity = 100;
            var left = 550;
            (Ti.Gesture.orientation == Titanium.UI.PORTRAIT || Ti.Gesture.orientation == Titanium.UI.UPSIDE_PORTRAIT) && (left = 287);
            var anim = getPanelAnimation(left);
            $.subContent.animate(anim);
            anim.addEventListener("complete", function() {
                $.content.animate(getPanelAnimation(75));
            });
            sub_panel_is_opened = true;
        },
        closePanel: function() {
            this.fireEvent("closePanel");
            var left = 1014;
            (Ti.Gesture.orientation == Titanium.UI.PORTRAIT || Ti.Gesture.orientation == Titanium.UI.UPSIDE_PORTRAIT) && (left = 760);
            var anim = getPanelAnimation(left);
            $.subContent.animate(anim);
            anim.addEventListener("complete", function() {
                $.content.animate(getPanelAnimation(287));
            });
            sub_panel_is_opened = false;
        },
        togglePanel: function() {
            true == sub_panel_is_opened ? this.closePanel() : this.openPanel();
        }
    };
    _.extend($.subContent, subPanel, {});
    var touchStartXPanel = 0;
    var touchStartedPanel = false;
    $.subContent.addEventListener("touchstart", function(event) {
        touchStartXPanel = parseInt(event.x, 10);
        touchStartedPanel = true;
    });
    $.subContent.addEventListener("touchend", function(event) {
        touchStartedPanel = false;
        event.source.convertPointToView({
            x: event.x,
            y: event.y
        }, $.slideMenu);
        var touchEndXPanel = parseInt(event.x, 10);
        var delta = touchEndXPanel - touchStartXPanel;
        if (0 == delta) return false;
        if ($.subContent.left > 0) {
            delta > 10 ? $.subContent.closePanel() : $.subContent.openPanel();
            -5 > delta ? $.subContent.openPanel() : $.subContent.closePanel();
        }
    });
    $.subContent.addEventListener("touchmove", function(event) {
        var coords = event.source.convertPointToView({
            x: event.x,
            y: event.y
        }, $.slideMenu);
        var _x = parseInt(coords.x, 10);
        var newLeft = _x - touchStartXPanel;
        var swipeToRight = newLeft > 0 ? true : false;
        if (touchStartedPanel) {
            $.subContent.left = newLeft;
            swipeToRight && $.subContent.width >= newLeft && ($.subContent.left = newLeft);
        }
        newLeft > 10 && (touchStartedPanel = true);
    });
    exports.init = function(options) {
        options.hasOwnProperty("leftDrawer") ? $.leftDrawer.initialize(options.leftDrawer) : $.slideMenu.remove($.leftDrawer);
        options.hasOwnProperty("subContent") ? $.subContent.initialize(options.subContent) : $.slideMenu.remove($.subContent);
        $.content.add(options.content);
        $.subContent.add(options.subContent);
    };
    exports.toggleLeftDrawer = function() {
        $.leftDrawer.toggleDrawer();
    };
    exports.toggleSubPanel = function() {
        $.subContent.togglePanel();
    };
    exports.openSubPanel = function() {
        $.subContent.openPanel();
    };
    exports.closeSubPanel = function() {
        $.subContent.closePanel();
    };
    Ti.Gesture.addEventListener("orientationchange", function(e) {
        Ti.API.info(Ti.Gesture.orientation);
        e.orientation == Titanium.UI.PORTRAIT || e.orientation == Titanium.UI.UPSIDE_PORTRAIT ? true == sub_panel_is_opened && $.subContent.animate(getPanelAnimation(287)) : (e.orientation == Titanium.UI.LANDSCAPE_LEFT || e.orientation == Titanium.UI.LANDSCAPE_RIGHT) && (true == sub_panel_is_opened ? $.subContent.animate(getPanelAnimation(550)) : $.subContent.animate(getPanelAnimation(1014)));
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;