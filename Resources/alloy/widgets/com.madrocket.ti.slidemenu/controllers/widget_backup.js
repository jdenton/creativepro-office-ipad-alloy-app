function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.madrocket.ti.slidemenu/" + s : s.substring(0, index) + "/com.madrocket.ti.slidemenu/" + s.substring(index + 1);
    return path;
}

function Controller() {
    new (require("alloy/widget"))("com.madrocket.ti.slidemenu");
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    exports.destroy = function() {};
    _.extend($, $.__views);
    var sub_panel_is_opened = false;
    var drawer = {
        is_opened: false,
        initialize: function(content) {
            this.setWidth(content.width);
            this.add(content);
        },
        openDrawer: function() {
            this.fireEvent("open");
            $.content.animate(this.getDrawerOpenAnimation());
            this.is_opened = true;
        },
        closeDrawer: function() {
            this.fireEvent("close");
            $.content.animate(this.getDrawerCloseAnimation());
            this.is_opened = false;
        },
        toggleDrawer: function() {
            this.is_opened ? this.closeDrawer() : this.openDrawer();
        }
    };
    _.extend($.leftDrawer, drawer, {
        getDrawerOpenAnimation: function() {
            var width = this.width;
            return Ti.UI.createAnimation({
                left: width,
                curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
                duration: 150
            });
        },
        getDrawerCloseAnimation: function() {
            return Ti.UI.createAnimation({
                left: 75,
                curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
                duration: 150
            });
        },
        getDrawerBumpAnimation: function(left) {
            return Ti.UI.createAnimation({
                left: left,
                curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
                duration: 150
            });
        }
    });
    var touchStartX = 0;
    var touchStarted = false;
    $.content.addEventListener("touchstart", function(event) {
        touchStartX = parseInt(event.x, 10);
        touchStarted = true;
    });
    $.content.addEventListener("touchend", function(event) {
        touchStarted = false;
        event.source.convertPointToView({
            x: event.x,
            y: event.y
        }, $.slideMenu);
        var touchEndX = parseInt(event.x, 10);
        var delta = touchEndX - touchStartX;
        if (0 == delta) return false;
        if ($.content.left > 0) {
            delta > 10 ? $.leftDrawer.openDrawer() : $.leftDrawer.closeDrawer();
            -5 > delta ? $.leftDrawer.closeDrawer() : $.leftDrawer.openDrawer();
        }
    });
    $.content.addEventListener("touchmove", function() {});
    $.leftDrawer.addEventListener("open", function() {
        $.trigger("open:[left]");
    });
    $.leftDrawer.addEventListener("close", function() {
        $.trigger("close:[left]");
    });
    var subPanel = {
        initialize: function(panelContent) {
            this.setWidth(panelContent.width);
            this.add(panelContent);
        },
        openPanel: function() {
            this.fireEvent("openPanel");
            $.subContent.opacity = 100;
            $.subContent.animate(this.getPanelOpenAnimation());
            $.content.animate($.leftDrawer.getDrawerBumpAnimation(75));
            sub_panel_is_opened = true;
        },
        closePanel: function() {
            this.fireEvent("closePanel");
            $.subContent.animate(this.getPanelCloseAnimation());
            sub_panel_is_opened = false;
            $.content.animate($.leftDrawer.getDrawerBumpAnimation(287));
        },
        togglePanel: function() {
            this.panel_is_opened ? this.closePanel() : this.openPanel();
        }
    };
    _.extend($.subContent, subPanel, {
        getPanelOpenAnimation: function() {
            var left = 550;
            3 > Ti.Gesture.orientation && (left = 287);
            return Ti.UI.createAnimation({
                left: left,
                curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
                duration: 150
            });
        },
        getPanelCloseAnimation: function() {
            return Ti.UI.createAnimation({
                left: 700,
                curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
                duration: 150,
                opacity: 0
            });
        },
        getPanelBumpAnimation: function(left) {
            return Ti.UI.createAnimation({
                left: left,
                curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
                duration: 150
            });
        }
    });
    var touchStartXPanel = 0;
    var touchStartedPanel = false;
    $.subContent.addEventListener("touchstart", function(event) {
        touchStartXPanel = parseInt(event.x, 10);
        touchStartedPanel = true;
    });
    $.subContent.addEventListener("touchend", function(event) {
        touchStartedPanel = false;
        event.source.convertPointToView({
            x: event.x,
            y: event.y
        }, $.slideMenu);
        var touchEndXPanel = parseInt(event.x, 10);
        var delta = touchEndXPanel - touchStartXPanel;
        if (0 == delta) return false;
        if ($.subContent.left > 0) {
            delta > 10 ? $.subContent.closePanel() : $.subContent.openPanel();
            -5 > delta ? $.subContent.openPanel() : $.subContent.closePanel();
        }
    });
    $.subContent.addEventListener("touchmove", function(event) {
        var coords = event.source.convertPointToView({
            x: event.x,
            y: event.y
        }, $.slideMenu);
        var _x = parseInt(coords.x, 10);
        var newLeft = _x - touchStartXPanel;
        var swipeToRight = newLeft > 0 ? true : false;
        if (touchStartedPanel) {
            $.subContent.left = newLeft;
            swipeToRight && $.subContent.width >= newLeft && ($.subContent.left = newLeft);
        }
        newLeft > 10 && (touchStartedPanel = true);
    });
    exports.init = function(options) {
        options.hasOwnProperty("leftDrawer") ? $.leftDrawer.initialize(options.leftDrawer) : $.slideMenu.remove($.leftDrawer);
        options.hasOwnProperty("subContent") ? $.subContent.initialize(options.subContent) : $.slideMenu.remove($.subContent);
        $.content.add(options.content);
        $.subContent.add(options.subContent);
    };
    exports.toggleLeftDrawer = function() {
        $.leftDrawer.toggleDrawer();
    };
    exports.toggleSubPanel = function() {
        $.subContent.togglePanel();
    };
    Ti.Gesture.addEventListener("orientationchange", function() {
        Ti.Gesture.orientation > 3 ? true == sub_panel_is_opened && $.subContent.animate($.subContent.getPanelBumpAnimation(550)) : 3 > Ti.Gesture.orientation;
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;