migration.up = function(migrator) {
	migrator.createTable({
		'columns': {
			'TimesheetId':    'INTEGER PRIMARY KEY',
			'ProjectId':      'INTEGER',
			'ProjectTitle':   'TEXT',
			'TaskId':         'INTEGER',
			'TaskTitle':      'TEXT',
			'Start':          'INTEGER',
			'Stop':           'INTEGER',
			'ElapsedTime':    'REAL',
			'DateClockStart': 'TEXT',
			'DateClockEnd':   'TEXT',
			'Billable':       'INTEGER',
			'Comments':       'TEXT'
		},
		'adapter': {
        	'type': 'sql',
        	'db_file': 'cpo',
            'collection_name': 'timesheet'
          }
	});
};

migration.down = function(migrator) {
	migrator.dropTable('timesheet');
};