migration.up = function(migrator) {
	migrator.createTable({
		'columns': {
			'UserId':      'INTEGER PRIMARY KEY',
			'Email':       'TEXT',
			'Password':    'TEXT',
			'AuthKey':     'TEXT',
			'Permissions': 'TEXT',
			'UserType':    'INTEGER'
		},
		'adapter': {
        	'type': 'sql',
			'db_file': 'cpo',
            'collection_name': 'login'
          }
	});
};

migration.down = function(migrator) {
	migrator.dropTable("login");
};