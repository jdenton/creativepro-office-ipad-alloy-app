var mainPanel = Alloy.createController('mainPanel');
var subPanel = Alloy.createController('subPanel');
var currentView;

// $.menu is a reference to the slidemenu widget
$.menu.init({
  leftDrawer:  Alloy.createController('leftMenu').getView(),
  content: mainPanel.getView(),
  subContent: subPanel.getView()
});

Ti.App.addEventListener('menuChange', function( data ) {
	var view = Alloy.createController(data.panel).getView();
	mainPanel.mainContent.add(view);
	if (currentView) {
		mainPanel.mainContent.remove(currentView);
	}
	currentView = view;
});

Ti.App.addEventListener('openSubPanel', function( data ) {
	$.menu.openSubPanel();
});	

Ti.App.addEventListener('closeSubPanel', function( data ) {
	$.menu.closeSubPanel();
});	

// By default, load our Projects view.
Ti.App.fireEvent( 'menuChange', { panel: 'projects' } );

/*
mainPanel.np.addEventListener('click', function() {
	$.menu.toggleSubPanel();
});
*/

$.index.open();
