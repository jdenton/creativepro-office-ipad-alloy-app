var isAuth = Alloy.Models.login.validateAuth();

var controller = 'login';
if (isAuth == true) {
	controller = 'home';
}
Alloy.createController(controller).getView().open();