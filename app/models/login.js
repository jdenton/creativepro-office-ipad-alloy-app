exports.definition = {
	config: {
		'columns': {
			'UserId':      'INTEGER PRIMARY KEY',
			'Email':       'TEXT',
			'Password':    'TEXT',
			'AuthKey':     'TEXT',
			'Permissions': 'TEXT',
			'UserType':    'INTEGER'
		},
		'adapter': {
			'type': 'sql',
			'collection_name': 'login',
            'idAttribute': 'UserId'
		}
	},

	extendModel : function(Model) {
        _.extend(Model.prototype, {
            login: function(username, password) {
                // Dummy authentication. In a real world scenario, this is
                // where you'd make a request to your authentication server or
                // other form of authentication. It would also likely return
                // the server time for loggedInSince, rather than using client-side
                // time like we are here.
                if (username === USERNAME && password === PASSWORD) {
                    this.set({
                        loggedIn: 1,
                        authKey: AUTHKEY
                    });
                    this.save();
                    return true;
                } else {
                    return false;
                }
            },
            logout: function() {
                this.set({
                    loggedIn: 0,
                    authKey: ''
                });
                this.save();
            },
            validateAuth: function() {
                // Again, this would be done against an auth server in a real world
                // scenario. We're just keeping it simple here.
                if (this.get('loggedIn') === 1 && this.get('authKey') === AUTHKEY) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        return Model;
    }
}