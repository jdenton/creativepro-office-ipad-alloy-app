exports.definition = {
	config: {
		'columns': {
			'TimesheetId':    'INTEGER PRIMARY KEY',
			'ProjectId':      'INTEGER',
			'ProjectTitle':   'TEXT',
			'TaskId':         'INTEGER',
			'TaskTitle':      'TEXT',
			'Start':          'INTEGER',
			'Stop':           'INTEGER',
			'ElapsedTime':    'REAL',
			'DateClockStart': 'TEXT',
			'DateClockEnd':   'TEXT',
			'Billable':       'INTEGER',
			'Comments':       'TEXT'
		},
		'adapter': {
			'type': 'sql',
			'collection_name': 'timesheet',
            'idAttribute': 'TimesheetId'
		}
	}
}	