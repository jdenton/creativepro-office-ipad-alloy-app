/*
 * This is for our left menn.
 */
var sub_panel_is_opened = false;

function getPanelAnimation(left) {
	return Ti.UI.createAnimation({
      left  : left,
      curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
      duration : 150
    });
}

var drawer = {
  is_opened: false,
  initialize: function(content) {
    this.setWidth(content.width);
    this.add(content);
  },
  openDrawer: function() {
    this.fireEvent('open');
    $.content.animate(getPanelAnimation(287));
    this.is_opened = true;
  },
  closeDrawer: function() {
    this.fireEvent('close');
    $.content.animate(getPanelAnimation(75));
    this.is_opened = false;
  },
  toggleDrawer: function() {
    if(this.is_opened) {
      this.closeDrawer();
    }
    else {
      this.openDrawer();
    }
  }
};

_.extend($.leftDrawer, drawer, {});

var touchStartX = 0;
var touchStarted = false;

/*
$.content.addEventListener('touchstart', function(event) {
  touchStartX = parseInt(event.x, 10);
  touchStarted = true;
});

$.content.addEventListener('touchend', function(event) {
  touchStarted = false;

  var coords = event.source.convertPointToView({x:event.x,y:event.y}, $.slideMenu);
  var touchEndX = parseInt(event.x, 10);

  var delta = touchEndX - touchStartX;

  if(delta == 0) { return false; }

  if($.content.left > 0) {
    if (delta > 10) {
      $.leftDrawer.openDrawer();
    } else {
      $.leftDrawer.closeDrawer();
    }
    if (delta < -5) {
      $.leftDrawer.closeDrawer();
    } else {
      $.leftDrawer.openDrawer();
    }
  }
});
*/

$.content.addEventListener('touchmove', function(event) {
	/*
  var coords = event.source.convertPointToView({x:event.x,y:event.y}, $.slideMenu);
  var _x = parseInt(coords.x, 10);
  var newLeft = _x - touchStartX;
  var swipeToRight = newLeft > 0 ? true : false;
  var swipeToLeft = newLeft < 0 ? true : false;
  if (touchStarted) {
    if ((swipeToRight && newLeft <= $.leftDrawer.width)) {
      $.content.left = newLeft;
    }
  }
  if (newLeft > 10) {
    touchStarted = true;
  }
  */
});

$.leftDrawer.addEventListener('open', function(){
  $.trigger('open:[left]');
});
$.leftDrawer.addEventListener('close', function(){
  $.trigger('close:[left]');
});

/*
 * This is for our subPanel
 */
var subPanel = {
  initialize: function(panelContent) {
    this.setWidth(panelContent.width);
    this.add(panelContent);
  },
  openPanel: function() {
    this.fireEvent('openPanel');
    $.subContent.opacity = 100;
    var left = 550;
    if (Ti.Gesture.orientation == Titanium.UI.PORTRAIT || Ti.Gesture.orientation == Titanium.UI.UPSIDE_PORTRAIT) {
  		left = 287;
  	}	
  	var anim = getPanelAnimation(left);
  	$.subContent.animate(anim);
    anim.addEventListener('complete', function() {
    	$.content.animate(getPanelAnimation(75));
    });
    sub_panel_is_opened = true;
  },
  closePanel: function() {
    this.fireEvent('closePanel');
    var left = 1014;
  	if (Ti.Gesture.orientation == Titanium.UI.PORTRAIT || Ti.Gesture.orientation == Titanium.UI.UPSIDE_PORTRAIT) {
  		left = 760;
  	}	
  	var anim = getPanelAnimation(left);
    $.subContent.animate(anim);
    anim.addEventListener('complete', function() {
    	$.content.animate(getPanelAnimation(287));
    });
    sub_panel_is_opened = false;
  },
  togglePanel: function() {
    if(sub_panel_is_opened == true) {
      this.closePanel();
    }
    else {
      this.openPanel();
    }
  }
};

_.extend($.subContent, subPanel, {});

var touchStartXPanel = 0;
var touchStartedPanel = false;

$.subContent.addEventListener('touchstart', function(event) {
  touchStartXPanel = parseInt(event.x, 10);
  touchStartedPanel = true;
});

$.subContent.addEventListener('touchend', function(event) {
  touchStartedPanel = false;

  var coords = event.source.convertPointToView({x:event.x,y:event.y}, $.slideMenu);
  var touchEndXPanel = parseInt(event.x, 10);

  var delta = touchEndXPanel - touchStartXPanel;

  if(delta == 0) { return false; }

  if($.subContent.left > 0) {
    if (delta > 10) {
      $.subContent.closePanel();
    } else {
      $.subContent.openPanel();
    }
    if (delta < -5) {
      $.subContent.openPanel();
    } else {
      $.subContent.closePanel();
    }
  }
});

$.subContent.addEventListener('touchmove', function(event) {
	  var coords = event.source.convertPointToView({x:event.x,y:event.y}, $.slideMenu);
	  var _x = parseInt(coords.x, 10);
	  var newLeft = _x - touchStartXPanel;
	  var swipeToRight = newLeft > 0 ? true : false;
	  var swipeToLeft = newLeft < 0 ? true : false;
	  if (touchStartedPanel) {
	  	$.subContent.left = newLeft;
	    if ((swipeToRight && newLeft <= $.subContent.width)) {
	      $.subContent.left = newLeft;
	    }
	  }
	  if (newLeft > 10) {
	    touchStartedPanel = true;
	  }
});

/*
 * Initialize the widget.
 */
exports.init = function(options) {
  if(options.hasOwnProperty('leftDrawer')) {
    $.leftDrawer.initialize(options.leftDrawer);
  }
  else {
    $.slideMenu.remove($.leftDrawer);
  }
  if(options.hasOwnProperty('subContent')) {
    $.subContent.initialize(options.subContent);
  }
  else {
    $.slideMenu.remove($.subContent);
  }

  $.content.add(options.content);
  $.subContent.add(options.subContent);
};

exports.toggleLeftDrawer = function(){ $.leftDrawer.toggleDrawer(); }
exports.toggleSubPanel = function(){ $.subContent.togglePanel(); }
exports.openSubPanel = function(){ $.subContent.openPanel(); }
exports.closeSubPanel = function(){ $.subContent.closePanel(); }

Ti.Gesture.addEventListener('orientationchange', function(e) {
	Ti.API.info(Ti.Gesture.orientation);
	if (e.orientation == Titanium.UI.PORTRAIT || e.orientation == Titanium.UI.UPSIDE_PORTRAIT) {
		if (sub_panel_is_opened == true) {
			$.subContent.animate(getPanelAnimation(287));
		}
	} else if (e.orientation == Titanium.UI.LANDSCAPE_LEFT || e.orientation == Titanium.UI.LANDSCAPE_RIGHT) {
		if (sub_panel_is_opened == true) {
			$.subContent.animate(getPanelAnimation(550));
		} else {
			$.subContent.animate(getPanelAnimation(1014));
		}
	}
});
